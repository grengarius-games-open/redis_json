#[cfg(test)]
mod tests;
mod deserializer;
mod error;
mod serializer;

pub use deserializer::{from_redis_value, from_redis_value_owned, Deserializer};
pub use error::{Error, Result};
pub use serializer::{to_redis_args, Serializer};

#[cfg(feature = "redis_json_derive")]
extern crate redis_json_derive;
#[cfg(feature = "redis_json_derive")]
pub use redis_json_derive::ToRedisArgs;

pub trait DeserializeRedisValue<'de, T> {
    fn deserialize(self) -> Result<T> where T: Sized + serde::de::DeserializeOwned;
}

impl<'de, T> DeserializeRedisValue<'de, T> for std::result::Result<redis::Value, redis::RedisError> {
    fn deserialize(self) -> Result<T> where T: Sized + serde::de::DeserializeOwned {
        match self {
            Ok(value) => {
                from_redis_value_owned(&value)
            }
            Err(what) => Err(Error::Redis(what))
        }
    }
}