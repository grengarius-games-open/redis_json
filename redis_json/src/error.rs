use serde::{de, ser};
use std::fmt::{self, Display};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    EmptyValue,
    EmptySequence,
    ExpectedBulk(String),
    ExpectedData(String),
    ExpectedDataOrBulk(String),
    ExpectedIdentifier(String),
    ExpectedValueAfterKey,
    ExpectedVariantTypeHeader(String),
    ExpectedVariantTypeName(String),
    Json(serde_json::Error),
    Message(String),
    Redis(redis::RedisError)
}

impl Error {
    pub fn new_expected_bulk(redis_value: &redis::Value) -> Error {
        Error::ExpectedData(format!("Expected Bulk, got {:?}", redis_value))
    }

    pub fn new_expected_data(redis_value: &redis::Value) -> Error {
        Error::ExpectedData(format!("Expected Data, got {:?}", redis_value))
    }

    pub fn new_expected_data_or_bulk(redis_value: &redis::Value) -> Error {
        Error::ExpectedData(format!("Expected Data or Bulk, got {:?}", redis_value))
    }

    pub fn new_expected_identifier(value: &str) -> Error {
        Error::ExpectedData(format!("Expected identifier, got {:?}", value))
    }

    pub fn new_expected_variant_type_header(value: &str) -> Error {
        Error::ExpectedData(format!("Expected variant type header, #variant_type, got {:?}", value))
    }

    pub fn new_expected_variant_type_name(value: &str) -> Error {
        Error::ExpectedData(format!("Expected variant type name, got {:?}", value))
    }

    pub fn new_json(error: serde_json::Error) -> Error {
        Error::Json(error)
    }

    pub fn new_redis(error: redis::RedisError) -> Error {
        Error::Redis(error)
    }
}

impl From<serde_json::Error> for Error {
    fn from(error: serde_json::Error) -> Error {
        Error::Json(error)
    }
}

impl ser::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Error::Message(msg.to_string())
    }
}

impl de::Error for Error {
    fn custom<T: Display>(msg: T) -> Self {
        Error::Message(msg.to_string())
    }
}

impl Display for Error {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(std::error::Error::description(self))
    }
}

impl std::error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::EmptyValue => "Empty value",
            Error::EmptySequence => "Empty sequence",
            Error::ExpectedData(ref msg) => msg,
            Error::ExpectedBulk(ref msg) => msg,
            Error::ExpectedDataOrBulk(ref msg) => msg,
            Error::ExpectedIdentifier(ref msg) => msg,
            Error::ExpectedValueAfterKey => "Expected value after key when deserializing map or struct",
            Error::ExpectedVariantTypeHeader(ref msg) => msg,
            Error::ExpectedVariantTypeName(ref msg) => msg,
            Error::Message(ref msg) => msg,
            Error::Json(ref what) => what.description(),
            Error::Redis(ref what) => what.description()
        }
    }
}
