use std::str;
use std::error::Error as StdError;

use serde::de::{
    self, DeserializeSeed, EnumAccess, IntoDeserializer, MapAccess, SeqAccess, VariantAccess,
    Visitor,
};
use serde::de::{Deserialize};

use super::error::{Error, Result};

type RedisValue = redis::Value;

pub struct Deserializer<'de> {
    input: &'de RedisValue,
    index: usize,
    is_map: bool
}

impl<'de> Deserializer<'de> {
    pub fn from_redis_value(input: &'de RedisValue) -> Self {
        Deserializer { input, index: 0, is_map: false }
    }
}

pub fn from_redis_value_owned<'a, T>(val: &'a RedisValue) -> Result<T>
where
    T: serde::de::DeserializeOwned,
{
    let mut deserializer = Deserializer::from_redis_value(val);
    T::deserialize(&mut deserializer)
}

pub fn from_redis_value<'a, T>(val: &'a RedisValue) -> Result<T>
where
    T: Deserialize<'a>,
{
    let mut deserializer = Deserializer::from_redis_value(val);
    T::deserialize(&mut deserializer)
}

impl<'de> Deserializer<'de> {
    fn get_json_deserializer(&self)
        -> Result<serde_json::Deserializer<serde_json::de::SliceRead<'de>>>
    {
        match self.input {
            RedisValue::Data(data) => Ok(serde_json::Deserializer::from_slice(data)),
            RedisValue::Nil => Err(Error::EmptyValue),
            _ => Err(Error::new_expected_data(self.input))
        }
    }
}

impl<'de, 'a> de::Deserializer<'de> for &'a mut Deserializer<'de> {
    type Error = Error;

    // Look at the input data to decide what Serde data model type to
    // deserialize as. Not all data formats are able to support this operation.
    // Formats that support `deserialize_any` are known as self-describing.
    fn deserialize_any<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_bool(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_bool<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_bool(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_i8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_i8(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_i16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_i16(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_i32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_i32(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_i64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_i64(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_u8<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_u8(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_u16<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_u16(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_u32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_u32(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_u64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_u64(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_f32<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_f32(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_f64<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_f64(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_char<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_char(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_str<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_str(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.deserialize_str(visitor)
    }

    fn deserialize_bytes<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_bytes(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_byte_buf<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_byte_buf(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_option<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_option(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_unit<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_unit(visitor)
            .map_err(Error::new_json)
    }

    fn deserialize_unit_struct<V>(self, name: &'static str, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.get_json_deserializer()?
            .deserialize_unit_struct(name, visitor)
            .map_err(Error::new_json)
    }

    // As is done here, serializers are encouraged to treat newtype structs as
    // insignificant wrappers around the data they contain. That means not
    // parsing anything other than the contained value.
    fn deserialize_newtype_struct<V>(self, _name: &'static str, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        visitor.visit_newtype_struct(self)
    }

    // Deserialization of compound types like sequences and maps happens by
    // passing the visitor an "Access" object that gives it the ability to
    // iterate through the data contained in the sequence.
    fn deserialize_seq<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.input {
            RedisValue::Data(value) => serde_json::Deserializer::from_slice(value)
                .deserialize_map(visitor)
                .map_err(|what| Error::Json(what)),
            // Value does not exist
            RedisValue::Nil => Err(Error::EmptyValue),
            // This might end up returning an empty sequence, but not an error.
            // As redis makes no distinction between a sequence that does not 
            // exist and one that is simply empty.
            RedisValue::Bulk(_) => visitor.visit_seq(self),
            _ => Err(Error::new_expected_data_or_bulk(self.input)),
        }
    }

    // Tuples look just like sequences in JSON. Some formats may be able to
    // represent tuples more efficiently.
    //
    // As indicated by the length parameter, the `Deserialize` implementation
    // for a tuple in the Serde data model is required to know the length of the
    // tuple before even looking at the input data.
    fn deserialize_tuple<V>(self, _len: usize, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.deserialize_seq(visitor)
    }

    // Tuple structs look just like sequences in JSON.
    fn deserialize_tuple_struct<V>(
        self,
        _name: &'static str,
        _len: usize,
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.deserialize_seq(visitor)
    }

    // Much like `deserialize_seq` but calls the visitors `visit_map` method
    // with a `MapAccess` implementation, rather than the visitor's `visit_seq`
    // method with a `SeqAccess` implementation.
    fn deserialize_map<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.is_map = true;
        match self.input {
            RedisValue::Data(value) => serde_json::Deserializer::from_slice(value)
                .deserialize_map(visitor)
                .map_err(|what| Error::Json(what)),
            RedisValue::Nil => Err(Error::EmptyValue),
            RedisValue::Bulk(_) => visitor.visit_map(self),
            _ => Err(Error::new_expected_data_or_bulk(self.input)),
        }
    }

    // Structs look just like maps in JSON.
    //
    // Notice the `fields` parameter - a "struct" in the Serde data model means
    // that the `Deserialize` implementation is required to know what the fields
    // are before even looking at the input data. Any key-value pairing in which
    // the fields cannot be known ahead of time is probably a map.
    fn deserialize_struct<V>(
        self,
        _name: &'static str,
        _fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {      
        match self.input {
            RedisValue::Data(value) => serde_json::Deserializer::from_slice(value)
                .deserialize_map(visitor)
                .map_err(|what| Error::Json(what)),
            RedisValue::Nil => Err(Error::EmptyValue),
            RedisValue::Bulk(values) => if values.len() > 0 {
                visitor.visit_map(self)
            } else {
                Err(Error::EmptySequence)
            },
            _ => Err(Error::new_expected_data_or_bulk(self.input)),
        }
    }

    fn deserialize_enum<V>(
        self,
        name: &'static str,
        variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.input {
            RedisValue::Data(_) => {
                self.get_json_deserializer()?
                    .deserialize_enum(name, variants, visitor)
                    .map_err(Error::new_json)
            },
            RedisValue::Nil => Err(Error::EmptyValue),
            RedisValue::Bulk(_) => visitor.visit_enum(Enum::new(self)),
            _ => Err(Error::new_expected_data_or_bulk(self.input)),
        }
    }

    // An identifier in Serde is the type that identifies a field of a struct or
    // the variant of an enum. In JSON, struct fields and enum variants are
    // represented as strings. In other formats they may be represented as
    // numeric indices.
    fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        match self.input {
            RedisValue::Bulk(values) => {
                if values.len() == 0 {
                    return Err(Error::EmptySequence)
                } else if values.len() == 1 {
                    return Err(Error::ExpectedVariantTypeName("Short sequence".to_owned()))
                }

                self.index = 2;
                if let RedisValue::Data(variant_type_header) = &values[0] {
                    match str::from_utf8(&variant_type_header) {
                        Ok("#variant_type") => Ok(()),
                        Ok(value) => Err(Error::new_expected_variant_type_header(value)),
                        Err(what) => Err(Error::new_expected_variant_type_header(what.description()))
                    }
                } else {
                    Err(Error::new_expected_data(&values[0]))
                }?;

                if let RedisValue::Data(variant_type_name) = &values[1] {
                    match str::from_utf8(&variant_type_name) {
                        Ok(variant_type_name) => visitor.visit_borrowed_str(variant_type_name),
                        Err(what) => Err(Error::new_expected_variant_type_name(what.description()))
                    }
                } else {
                    Err(Error::new_expected_data(&values[1]))
                }
            }
            RedisValue::Data(value) => {
                match str::from_utf8(value) {
                    Ok(identifier) => visitor.visit_borrowed_str(identifier),
                    Err(what) => Err(Error::new_expected_identifier(what.description()))
                }
            }
            RedisValue::Nil => Err(Error::EmptyValue),
            _ => Err(Error::new_expected_data_or_bulk(self.input)),
        }
    }

    // Like `deserialize_any` but indicates to the `Deserializer` that it makes
    // no difference which `Visitor` method is called because the data is
    // ignored.
    //
    // Some deserializers are able to implement this more efficiently than
    // `deserialize_any`, for example by rapidly skipping over matched
    // delimiters without paying close attention to the data in between.
    //
    // Some formats are not able to implement this at all. Formats that can
    // implement `deserialize_any` and `deserialize_ignored_any` are known as
    // self-describing.
    fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.deserialize_any(visitor)
    }
}

// `SeqAccess` is provided to the `Visitor` to give it the ability to iterate
// through elements of the sequence.
impl<'de> SeqAccess<'de> for Deserializer<'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        match self.input {
            RedisValue::Bulk(values) => {
                if self.index < values.len() {
                    let mut deserializer = Deserializer::from_redis_value(&values[self.index]);
                    self.index += 1;
                    seed.deserialize(&mut deserializer).map(Some)
                } else {
                    //End of sequence
                    Ok(None)
                }
            }
            _ => Err(Error::new_expected_bulk(self.input)),
        }
    }
}

// `MapAccess` is provided to the `Visitor` to give it the ability to iterate
// through entries of the map.
impl<'de> MapAccess<'de> for Deserializer<'de> {
    type Error = Error;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
    where
        K: DeserializeSeed<'de>,
    {
        match self.input {
            RedisValue::Bulk(values) => {
                if self.index < values.len() {
                    // Maps have json keys.
                    if self.is_map {
                        let mut deserializer = Deserializer::from_redis_value(&values[self.index]);
                        self.index += 1;
                        seed.deserialize(&mut deserializer).map(Some)
                    // Structs have plain string keys.
                    } else {
                        if let RedisValue::Data(value) = &values[self.index] {
                            let value = str::from_utf8(&value)
                                .map_err(|what| Error::new_expected_identifier(what.description()))?;

                            self.index += 1;
                            seed.deserialize(value.into_deserializer()).map(Some)
                        } else {
                            Err(Error::new_expected_data(&values[self.index]))
                        }
                    }
                } else {
                    //End of map
                    Ok(None)
                }
            }
            _ => Err(Error::new_expected_bulk(self.input))
        }
    }

    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
    where
        V: DeserializeSeed<'de>,
    {
        match self.input {
            RedisValue::Bulk(values) => {
                let mut deserializer = Deserializer::from_redis_value(&values[self.index]);
                self.index += 1;
                seed.deserialize(&mut deserializer)
            }
            _ => Err(Error::new_expected_bulk(self.input))
        }
    }
}

struct Enum<'a, 'de: 'a> {
    de: &'a mut Deserializer<'de>,
}

impl<'a, 'de> Enum<'a, 'de> {
    fn new(de: &'a mut Deserializer<'de>) -> Self {
        Enum { de }
    }
}

// `EnumAccess` is provided to the `Visitor` to give it the ability to determine
// which variant of the enum is supposed to be deserialized.
//
// Note that all enum deserialization methods in Serde refer exclusively to the
// "externally tagged" enum representation.
impl<'de, 'a> EnumAccess<'de> for Enum<'a, 'de> {
    type Error = Error;
    type Variant = Self;

    fn variant_seed<V>(self, seed: V) -> Result<(V::Value, Self::Variant)>
    where
        V: DeserializeSeed<'de>,
    {
        let val = seed.deserialize(&mut *self.de)?;

        Ok((val, self))
    }
}

// `VariantAccess` is provided to the `Visitor` to give it the ability to see
// the content of the single variant that it decided to deserialize.
impl<'de, 'a> VariantAccess<'de> for Enum<'a, 'de> {
    type Error = Error;

    // If the `Visitor` expected this variant to be a unit variant, the input
    // should have been the plain string case handled in `deserialize_enum`.
    fn unit_variant(self) -> Result<()> {
        Ok(())
    }

    // Newtype variants are represented in JSON as `{ NAME: VALUE }` so
    // deserialize the value here.
    fn newtype_variant_seed<T>(self, seed: T) -> Result<T::Value>
    where
        T: DeserializeSeed<'de>,
    {
        match self.de.input {
            RedisValue::Bulk(values) => {
                if values.len() == 3 {
                    seed.deserialize(&mut Deserializer::from_redis_value(&values[2]))
                } else {
                    seed.deserialize(&mut *self.de)
                }
            }
            _ => Err(Error::new_expected_bulk(self.de.input)),
        }
    }

    // Tuple variants are represented in JSON as `{ NAME: [DATA...] }` so
    // deserialize the sequence of data here.
    fn tuple_variant<V>(self, _len: usize, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        de::Deserializer::deserialize_seq(&mut *self.de, visitor)
    }

    // Struct variants are represented in JSON as `{ NAME: { K: V, ... } }` so
    // deserialize the inner map here.
    fn struct_variant<V>(self, _fields: &'static [&'static str], visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        de::Deserializer::deserialize_map(&mut *self.de, visitor)
    }
}
