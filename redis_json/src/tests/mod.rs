mod deserializer;
mod serializer;

use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct Struct1 {
    val1: String,
    val2: bool,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct NestedStruct {
    cool_string: String,
    nested: Struct1,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
struct StructWithEnum {
    val1: i32,
    val2: bool,
    val3: Enum1,
    val4: String
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
enum Enum1 {
    UnitVariant,
    NewtypeVariant1(String),
    NewtypeVariant2(Struct1),
    TupleVariant(u32, u32, u32),
    StructVariant { val1: i64, val2: f32 },
}