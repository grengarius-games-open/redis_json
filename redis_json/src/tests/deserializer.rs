use std::collections::HashMap;
use std::collections::HashSet;
use super::*;
use crate::Error;

#[test]
fn deserialize_i32() {
    let val = redis::Value::Data("1".as_bytes().to_owned());

    let de_val: i32 = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, 1)
}

#[test]
fn deserialize_string() {
    let val = redis::Value::Data("\"hue\"".as_bytes().to_owned());

    let de_val: String = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, "hue")
}

#[test]
fn deserialize_vector() {
    let expected = vec![1, 2, 3, 4, 5];

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("1".as_bytes().to_owned()),
        redis::Value::Data("2".as_bytes().to_owned()),
        redis::Value::Data("3".as_bytes().to_owned()),
        redis::Value::Data("4".as_bytes().to_owned()),
        redis::Value::Data("5".as_bytes().to_owned()),
    ]);

    let de_val: Vec<i32> = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, expected)
}
#[test]
fn deserialize_hashset() {
    let expected: HashSet<i32> = [1, 2, 3, 4, 5].iter().cloned().collect();

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("1".as_bytes().to_owned()),
        redis::Value::Data("2".as_bytes().to_owned()),
        redis::Value::Data("3".as_bytes().to_owned()),
        redis::Value::Data("4".as_bytes().to_owned()),
        redis::Value::Data("5".as_bytes().to_owned()),
    ]);

    let de_val: HashSet<i32> = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, expected)
}

#[test]
fn deserialize_dictionary() {
    let expected: HashMap<String, String> = [
        ("test".to_owned(), "hue".to_owned()),
        ("test2".to_owned(), "hue2".to_owned()),
    ]
    .iter()
    .cloned()
    .collect();

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("test".as_bytes().to_owned()),
        redis::Value::Data("\"hue\"".as_bytes().to_owned()),
        redis::Value::Data("test2".as_bytes().to_owned()),
        redis::Value::Data("\"hue2\"".as_bytes().to_owned()),
    ]);

    let de_val: HashMap<String, String> = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, expected)
}

#[test]
fn deserialize_nested_structs() {
    let expected = NestedStruct {
        cool_string: "hue".to_owned(),
        nested: Struct1 {
            val1: "br".to_owned(),
            val2: false,
        },
    };

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("cool_string".as_bytes().to_owned()),
        redis::Value::Data("\"hue\"".as_bytes().to_owned()),
        redis::Value::Data("nested".as_bytes().to_owned()),
        redis::Value::Data("{\"val1\":\"br\",\"val2\":false}".as_bytes().to_owned()),
    ]);

    let de_val: NestedStruct = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, expected);
}


#[test]
fn deserialize_unit_variant() {
    let expected = Enum1::UnitVariant;

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("#variant_type".as_bytes().to_owned()),
        redis::Value::Data("UnitVariant".as_bytes().to_owned()),
    ]);

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_newtype_variant1() {
    let expected = Enum1::NewtypeVariant1("batata".to_owned());
    let val = redis::Value::Bulk(vec![
        redis::Value::Data("#variant_type".as_bytes().to_owned()),
        redis::Value::Data("NewtypeVariant1".as_bytes().to_owned()),
        redis::Value::Data("\"batata\"".as_bytes().to_owned()),
    ]);

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();
    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_newtype_variant2() {
    let expected = Enum1::NewtypeVariant2(Struct1 {
        val1: "batata".to_owned(),
        val2: true,
    });

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("#variant_type".as_bytes().to_owned()),
        redis::Value::Data("NewtypeVariant2".as_bytes().to_owned()),
        redis::Value::Data("val1".as_bytes().to_owned()),
        redis::Value::Data("\"batata\"".as_bytes().to_owned()),
        redis::Value::Data("val2".as_bytes().to_owned()),
        redis::Value::Data("true".as_bytes().to_owned()),
    ]);

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();
    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_tuple_struct() {
    let expected = Enum1::TupleVariant(100, 255, 0);

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("#variant_type".as_bytes().to_owned()),
        redis::Value::Data("TupleVariant".as_bytes().to_owned()),
        redis::Value::Data("100".as_bytes().to_owned()),
        redis::Value::Data("255".as_bytes().to_owned()),
        redis::Value::Data("0".as_bytes().to_owned()),
    ]);

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();
    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_struct_variant() {
    let expected = Enum1::StructVariant {
        val1: -20,
        val2: 1.051,
    };

    let val = redis::Value::Bulk(vec![
        redis::Value::Data("#variant_type".as_bytes().to_owned()),
        redis::Value::Data("StructVariant".as_bytes().to_owned()),
        redis::Value::Data("val1".as_bytes().to_owned()),
        redis::Value::Data("-20".as_bytes().to_owned()),
        redis::Value::Data("val2".as_bytes().to_owned()),
        redis::Value::Data("1.051".as_bytes().to_owned()),
    ]);

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_unit_variant_json() {
    let expected = Enum1::UnitVariant;

    let val = redis::Value::Data("\"UnitVariant\"".as_bytes().to_owned());

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();

    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_newtype_variant1_json() {
    let expected = Enum1::NewtypeVariant1("batata".to_owned());

    let val = redis::Value::Data(
        "{\"NewtypeVariant1\":\"batata\"}"
        .as_bytes().to_owned());

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();
    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_newtype_variant2_json() {
    let expected = Enum1::NewtypeVariant2(Struct1 {
        val1: "batata".to_owned(),
        val2: true,
    });

    let val = redis::Value::Data(
        "{\"NewtypeVariant2\":{\"val1\":\"batata\",\"val2\":true}}"
        .as_bytes().to_owned());


    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();
    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_tuple_struct_json() {
    let expected = Enum1::TupleVariant(100, 255, 0);

    let val = redis::Value::Data(
        "{\"TupleVariant\":[100,255,0]}"
        .as_bytes().to_owned());

    let de_val: Enum1 = crate::from_redis_value(&val).unwrap();
    assert_eq!(de_val, expected);
}

#[test]
fn deserialize_struct_wrong_types() {
    let val = redis::Value::Bulk(vec![
        redis::Value::Data("val1".as_bytes().to_owned()),
        redis::Value::Data("true".as_bytes().to_owned()),
        redis::Value::Data("val2".as_bytes().to_owned()),
        redis::Value::Data("\"true\"".as_bytes().to_owned()),
    ]);

    let result: crate::Result<Struct1> = crate::from_redis_value(&val);

    assert!(result.is_err(), "Should not have deserialized incompatible struct")
}

#[test]
fn deserialize_struct_empty_sequence() {
    let val = redis::Value::Bulk(vec![]);

    let result: crate::Result<Struct1> = crate::from_redis_value(&val);

    match result {
        Err(Error::EmptySequence) => assert!(true),
        Err(what) => assert!(false, format!("Expected empty sequence, got {:?}", what)),
        _ => assert!(false, "Expected empty sequence, deserialized struct somehow")
    }
}

#[test]
fn deserialize_string_empty_empty_value() {
    let val = redis::Value::Nil;

    let result: crate::Result<String> = crate::from_redis_value(&val);

    match result {
        Err(Error::EmptyValue) => assert!(true),
        Err(what) => assert!(false, format!("Expected empty value, got {:?}", what)),
        _ => assert!(false, "Expected empty value, deserialized somehow")
    }
}

#[test]
fn deserialize_newtype_variant1_empty_sequence() {
    let val = redis::Value::Bulk(vec![]);

    let result: crate::Result<Enum1> = crate::from_redis_value(&val);

    match result {
        Err(Error::EmptySequence) => assert!(true),
        Err(what) => assert!(false, format!("Expected empty sequence, got {:?}", what)),
        _ => assert!(false, "Expected empty sequence, deserialized somehow")
    }
}

#[test]
fn deserialize_vec_empty_sequence() {
    let val = redis::Value::Bulk(vec![]);

    let result: crate::Result<Vec<i32>> = crate::from_redis_value(&val);

    match result {
        Ok(vec) => assert!(vec.len() == 0, format!("Expected empty vector, got non empty vector {:?}", vec)),
        Err(Error::EmptySequence) => assert!(false, "Expected empty vector, got Error::EmptySequence"),
        Err(what) => assert!(false, format!("Expected empty vector, got {:?}", what))
    }
}

#[test]
fn deserialize_vec_empty_value() {
    let val = redis::Value::Nil;

    let result: crate::Result<Vec<i32>> = crate::from_redis_value(&val);

    match result {
        Err(Error::EmptyValue) => assert!(true),
        Err(what) => assert!(false, format!("Expected empty value, got {:?}", what)),
        Ok(what) => assert!(false, format!("Expected empty value, got {:?}", what))
    }
}

#[test]
fn deserialize_map_empty_sequence() {
    let val = redis::Value::Bulk(vec![]);

    let result: crate::Result<HashMap<i32, i32>> = crate::from_redis_value(&val);

    match result {
        Ok(map) => assert!(map.len() == 0, format!("Expected empty map, got non empty map {:?}", map)),
        Err(Error::EmptySequence) => assert!(false, "Expected empty map, got Error::EmptySequence"),
        Err(what) => assert!(false, format!("Expected empty map, got {:?}", what))
    }
}

#[test]
fn deserialize_map_empty_value() {
    let val = redis::Value::Nil;

    let result: crate::Result<HashMap<i32, i32>> = crate::from_redis_value(&val);

    match result {
        Err(Error::EmptyValue) => assert!(true),
        Err(what) => assert!(false, format!("Expected empty value, got {:?}", what)),
        Ok(what) => assert!(false, format!("Expected empty value, got {:?}", what))
    }
}