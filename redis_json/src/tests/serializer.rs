use super::*;
use std::str;

fn to_str_vec<'a>(input: &'a Vec<Vec<u8>>) -> Vec<&'a str> {
    let mut out: Vec<&'a str> = Vec::new();

    for elem in input.iter() {
        out.push(str::from_utf8(&elem).unwrap())
    }

    out
}

#[test]
fn serialize_unit_variant() {
    let val = Enum1::UnitVariant;

    let expected = vec!["\"UnitVariant\""];

    let se_val = crate::to_redis_args(&val).unwrap();

    assert_eq!(to_str_vec(&se_val), expected)
}

#[test]
fn serialize_struct() {
    let val = Struct1 {
        val1: "br".to_owned(),
        val2: false,
    };

    let expected = vec![
        "val1",
        "\"br\"",
        "val2",
        "false",
    ];

    let se_val = crate::to_redis_args(&val).unwrap();

    assert_eq!(to_str_vec(&se_val), expected)
}

#[test]
fn serialize_struct_with_struct_variant() {
    let val = StructWithEnum {
        val1: 1,
        val2: false,
        val3: Enum1::StructVariant {
            val1: 20, val2: 0.5
        },
        val4: "Hue".to_owned()
    };

    let expected = vec![
        "val1",
        "1",
        "val2",
        "false",
        "val3",
        "{\"StructVariant\":{\"val1\":20,\"val2\":0.5}}",
        "val4",
        "\"Hue\""
    ];

    let se_val = crate::to_redis_args(&val).unwrap();

    assert_eq!(to_str_vec(&se_val), expected)
}

fn serialize_nested_struct() {
    let val = NestedStruct {
        cool_string: "Hue".to_owned(),
        nested: Struct1 {
            val1: "br".to_owned(),
            val2: true,
        },
    };

    let expected = vec![
        "cool_string",
        "\"Hue\"",
        "nested",
        "{\"val1\":\"br\",\"val2\":true}",
    ];

    let se_val = crate::to_redis_args(&val).unwrap();

    assert_eq!(to_str_vec(&se_val), expected)
}