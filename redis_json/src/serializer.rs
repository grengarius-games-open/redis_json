use super::error::{Error, Result};
use serde::{ser, Serialize};
use std::vec::Vec;
use std::str;

pub struct Serializer {
    output: Vec<Vec<u8>>,
}

pub fn to_redis_args<T>(value: &T) -> Result<Vec<Vec<u8>>>
where
    T: Serialize,
{
    let mut serializer = Serializer { output: Vec::new() };

    value.serialize(&mut serializer)?;
    Ok(serializer.output)
}

impl Serializer {
    fn serialize_any<T>(&mut self, v: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        let v = serde_json::to_vec(v).map_err(|what| Error::Json(what))?;

        self.output.push(v);

        Ok(())
    }
}

impl<'a> ser::Serializer for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    type SerializeSeq = Self;
    type SerializeTuple = Self;
    type SerializeTupleStruct = Self;
    type SerializeTupleVariant = Self;
    type SerializeMap = Self;
    type SerializeStruct = Self;
    type SerializeStructVariant = Self;

    fn serialize_bool(self, v: bool) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_i8(self, v: i8) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_i16(self, v: i16) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_i32(self, v: i32) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_i64(self, v: i64) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_u8(self, v: u8) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_u16(self, v: u16) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_u32(self, v: u32) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_u64(self, v: u64) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_f32(self, v: f32) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_f64(self, v: f64) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_char(self, v: char) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_str(self, v: &str) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_bytes(self, v: &[u8]) -> Result<()> {
        self.serialize_any(&v)
    }

    fn serialize_none(self) -> Result<()> {
        self.serialize_unit()
    }

    fn serialize_some<T>(self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(self)
    }

    // In Serde, unit means an anonymous value containing no data. Map this to
    // JSON as `null`.
    fn serialize_unit(self) -> Result<()> {
        self.output.push(String::from("null").as_bytes().to_owned());

        Ok(())
    }

    // Unit struct means a named value containing no data. Again, since there is
    // no data, map this to JSON as `null`. There is no need to serialize the
    // name in most formats.
    fn serialize_unit_struct(self, _name: &'static str) -> Result<()> {
        self.serialize_unit()
    }

    // When serializing a unit variant (or any other kind of variant), formats
    // can choose whether to keep track of it by index or by name. Binary
    // formats typically use the index of the variant and human-readable formats
    // typically use the name.
    fn serialize_unit_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
    ) -> Result<()> {
        //Unit variants in json are serialized as strings.
        self.serialize_str(variant)
    }

    // As is done here, serializers are encouraged to treat newtype structs as
    // insignificant wrappers around the data they contain.
    fn serialize_newtype_struct<T>(self, _name: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        value.serialize(self)
    }

    // Note that newtype variant (and all of the other variant serialization
    // methods) refer exclusively to the "externally tagged" enum
    // representation.
    fn serialize_newtype_variant<T>(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        value: &T,
    ) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        //Add a field to tag the variant
        self.output.push("#variant_type".as_bytes().to_owned());
        self.output.push(variant.as_bytes().to_owned());
        //Serialize the value itself
        self.serialize_any(&value)
    }

    fn serialize_seq(self, _len: Option<usize>) -> Result<Self::SerializeSeq> {
        Ok(self)
    }

    // Tuple look just like sequences.
    fn serialize_tuple(self, _len: usize) -> Result<Self::SerializeTuple> {
        Ok(self)
    }

    // Tuple structs look just like sequences.
    fn serialize_tuple_struct(
        self,
        _name: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleStruct> {
        Ok(self)
    }

    // Tuple variants are represented in JSON as `{ NAME: [DATA...] }`. Again
    // this method is only responsible for the externally tagged representation.
    fn serialize_tuple_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeTupleVariant> {
        //Add a field to tag the variant
        self.output.push("#variant_type".as_bytes().to_owned());
        self.output.push(variant.as_bytes().to_owned());
        Ok(self)
    }

    // Maps are represented as sequences, even values are keys, odd ones are values
    fn serialize_map(self, _len: Option<usize>) -> Result<Self::SerializeMap> {
        Ok(self)
    }

    // Structs are represented as sequences, even values are keys, odd ones are values
    fn serialize_struct(self, _name: &'static str, _len: usize) -> Result<Self::SerializeStruct> {
        // self.serialize_map(Some(len))
        Ok(self)
    }

    // This is supposed to be the externally tagged representation,
    // however as it's going to end up as sequence of keys and values,
    // we add a variant type field with a name that won't conflict with a valid struct field name
    fn serialize_struct_variant(
        self,
        _name: &'static str,
        _variant_index: u32,
        variant: &'static str,
        _len: usize,
    ) -> Result<Self::SerializeStructVariant> {
        //Add a field to tag the variant
        self.output.push("#variant_type".as_bytes().to_owned());
        self.output.push(variant.as_bytes().to_owned());

        Ok(self)
    }
}

// The following 7 impls deal with the serialization of compound types like
// sequences and maps. Serialization of such types is begun by a Serializer
// method and followed by zero or more calls to serialize individual elements of
// the compound type and one call to end the compound type.
//
// This impl is SerializeSeq so these methods are called after `serialize_seq`
// is called on the Serializer.
impl<'a> ser::SerializeSeq for &'a mut Serializer {
    // Must match the `Ok` type of the serializer.
    type Ok = ();
    // Must match the `Error` type of the serializer.
    type Error = Error;

    // Serialize a single element of the sequence.
    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.serialize_any(value)
    }

    // Close the sequence.
    fn end(self) -> Result<()> {
        Ok(())
    }
}

// Same thing but for tuples.
impl<'a> ser::SerializeTuple for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_element<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.serialize_any(value)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

// Same thing but for tuple structs.
impl<'a> ser::SerializeTupleStruct for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.serialize_any(value)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a> ser::SerializeTupleVariant for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.serialize_any(value)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a> ser::SerializeMap for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_key<T>(&mut self, key: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        key.serialize(&mut **self)?;

        Ok(())
    }

    fn serialize_value<T>(&mut self, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.serialize_any(value)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a> ser::SerializeStruct for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        self.output.push(key.as_bytes().to_vec());
        self.serialize_any(value)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}

impl<'a> ser::SerializeStructVariant for &'a mut Serializer {
    type Ok = ();
    type Error = Error;

    fn serialize_field<T>(&mut self, key: &'static str, value: &T) -> Result<()>
    where
        T: ?Sized + Serialize,
    {
        key.serialize(&mut **self)?;
        self.serialize_any(value)
    }

    fn end(self) -> Result<()> {
        Ok(())
    }
}