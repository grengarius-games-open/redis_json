extern crate proc_macro;
use crate::proc_macro::TokenStream;
use quote::quote;
use syn;

// #[proc_macro_derive(FromRedisValue)]
// pub fn from_redis_value_derive(input: TokenStream) -> TokenStream {
//     let ast: syn::DeriveInput = syn::parse(input).unwrap();

//     let name = &ast.ident;
//     let gen = quote! {
//         impl redis::FromRedisValue for #name {
//             fn from_redis_value(v: &redis::Value) -> redis::RedisResult<#name> {
//                 use std::error::Error;

//                 println!("Value: {:?}", v);

//                 redis_json::from_redis_value(v)
//                     .map_err(|what|
//                         (redis::ErrorKind::TypeError, "Deserialization failed", what.description().to_owned()).into())
//             }
//         }
//     };
//     gen.into()
// }

#[proc_macro_derive(ToRedisArgs)]
pub fn to_redis_args_derive(input: TokenStream) -> TokenStream {
    let ast: syn::DeriveInput = syn::parse(input).unwrap();

    let name = &ast.ident;
    let gen = quote! {
        impl redis::ToRedisArgs for &#name {
            fn write_redis_args<W>(&self, out: &mut W)
            where
                W: ?Sized + redis::RedisWrite,
            {
                let args = redis_json::to_redis_args(self)
                    .expect("Serialization failed");

                for arg in &args {
                    out.write_arg(arg);
                }
            }
        }
    };
    gen.into()
}